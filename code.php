<?php 
	// Variables
	// Variables are defined using the dollar ($) notation before the name of the variable
	$name = 'John Smith';
	$email = 'johnsmith@gmail.com';

	// Constant
	define('PI', 3.1416);

	// Data Types
		//Strings
			$state = 'New York';
			$country = 'United States of America';
			// $address = $state. ', ' .$country; concatenation via dot sign
			$address = "$state, $country";

		// Integers
			$age = 31;
			$headcount =26;

		//decimal
			$grade = 98.2;
			$distanceInKilometers = 1324.34;

		//boolean
			$hasTravelledAbroad = false;
			$haveSymptoms = true;

		// null
			$boyfriend =null;
			$girlfriend = null;

		//Arrays
			$grades = array(90.1, 87, 98, 93);

		// Objects
			$gradesObj = (object) [
				'firstGrading' => 98.7,
				'secondGrading' => 92.1,
				'thirdGrading' => 90.2,
				'fourthGrading'=> 95
			];

			$personObj = (object) [
				'fullName' => 'John Doe',
				'isMarried' => false,
				'age' => 35,
				'address' => (object) [
					'state' => 'New York',
					'country' => 'United States of America'
				]
			];

	// Operators
			$x = 1342.14;
			$y = 1268.24;

			$isLegalAge = true;
			$isRegistered = false;

	// functions
			function getFullName($firstName ="user", $middleInitial="user", $lastName = "user"){

				return "$lastName, $firstName, $middleInitial";
			}


	// Selection Control Structures

		// If-elseIf-Else Statement

			function determineTyphoonIntensity($windSpeed){
				if($windSpeed < 30){
					return 'Not a typhoon';
				}
				else if ($windSpeed <= 61) {
					return 'Tropical depression detected';
				}
				else if ($windSpeed >= 62 && $windSPeed <= 88){
					return 'Tropical storm detected';
				}
				else if ($windSpeed >= 89 && $windSpeed <= 117) {
					return 'Severe tropical storm detected.';
				}
				else {
					return 'Typhoon detected.';
				}
			}

	// Conditional (terbary) Operator

		function isUnderAge($age){
			// Ternary or one liner
			return ($age < 18)? true : false;
		}

	// Switch statements
		function determineComputerUser($computerNumber) {
			switch ($computerNumber){
				case 1:
					return 'Linus Torvalds';
					break;
				case 2:
					return 'Steve Harvey';
					break;
				case 3: 
					return 'Sid Meier';
					break;
				case 4:
					return 'Onel De Guzaman';
					break;
				case 5:
					return 'Christopher Mortel';
					break;
				default:
					return $computerNumber. ' is out of bounds.';
					break;
			}
		}

	//Try-Catch-Finally Statement
	function greeting($str){
		try{
			// Attempt to execute a code.
			if(gettype($str) =="string"){
				echo $str;
			}else{
				throw new Exception("Oops di kayo match!");
			}
		}

		catch (Exception $e){
			// catch the errors within the "try"
			echo $e->getMessage();
		}
		finally{
			// continue of execution of code regatdless of success and failure of code execution
			echo "wala talaga kayo";
		}
	}

	/*Activity for S1 - 12/19/2022*/
	function getFullAddress($address, $city, $province, $country){
		if(gettype($address) == "string" && gettype($city) == "string" && gettype($province) == "string" && gettype($country) == "string"){

			return $address. ', ' .$city. ', ' .$province. ', ' .$country;
		}else{

			return 'You must enter correct address.';
		}
	}

	/*Activity 2 S1*/

	function getLetterGrade($grade){
		if($grade > 100){
			return 'Grade is invalid! Please try again.';
		}else if ($grade > 97){
			return 'A+';
		}else if($grade > 94){
			return 'A';
		}else if($grade > 91){
			return 'A-';
		}else if($grade > 88){
			return 'B+';
		}else if($grade > 85){
			return 'B';
		}else if($grade > 82){
			return 'B-';
		}else if($grade > 79){
			return 'C+';
		}else if($grade > 76){
			return 'C';
		}else if($grade > 74){
			return 'C-';
		}else{
			return 'D';
		}
	}


?>
