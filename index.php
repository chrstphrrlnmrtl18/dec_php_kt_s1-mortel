<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s01: PHP Basics and Selection Control Structures</title>
	</head>

	<body>

		<!-- <h1>Hello World!</h1> -->

		<h1>Echoing Values</h1>
		<!-- Variables can be used to output  -->

		<p>
			<?php echo 'Good day, $name! Your given email is $email.' ?>
		</p>

		<p>
			<?php echo "Good day, $name! Your given email is $email." ?>
		</p>

		<p>
			<?php echo PI ?>
		</p>

		<p>
			<?php echo $address ?>
			<?php echo $age ?>
			<?php echo $headcount ?>
			


		</p>
		<!-- To output the value of an object property, the arrow notation can be used. -->
		<p>
			<?php echo $gradesObj->firstGrading; ?>
			<?php echo $personObj->address->state; ?>
		</p>

		<p>
			<?php echo $hasTravelledAbroad; ?>
		</p>

		<p>
			<?php echo $girlfriend; ?>
		</p>

		<p><?php echo gettype($hasTravelledAbroad); ?></p>
		<!-- Var_dump see more details info on the variable -->
		<p><?php echo var_dump($girlfriend); ?></p>

		<p><?php echo $grades[2]; ?></p>


		<h1>Operators</h1>

		<h2>Arithmetic Operators</h2>

		<p>Sum: <?php echo $x + $y?></p>

		<h2>Equality operators</h2>

		<p>Loose Equality: <?php echo var_dump($x == '1342.14'); ?></p>
		<p>Loose Equality: <?php echo var_dump($x != '1342.14'); ?></p>

		<h2>Greater/lesser Operators</h2>
		<p>isGreater: <?php echo var_dump($x > $y); ?></p>

		<h2>Logical Operators</h2>

		<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>

		<h1>Functions</h1>
		<p>Full Name: <?php echo getFullName("john", "doe", "hello"); ?></p>

		<h1>Selection Control Structures</h1>
		<p>Typhoon State: <?php echo determineTyphoonIntensity(35);?></p>

		<h1>Ternary Operator</h1>
		<p>Ternary Operator: <?php echo var_dump(isUnderAge(78)); ?></p>

		<h1>Switch case</h1>
		<p>Switch case result: <?php echo determineComputerUser(1); ?></p>

		<h1>Try catch</h1>
		<p><?php greeting(1); ?></p>

		<h1>Activity 1:</h1>
		<p>Result from Activity 1: </p>
		<p><?php echo getFullAddress('hi', 'hi2', 'hi3', 'hi4'); ?></p>

		<h1>Activity 2:</h1>
		<p>Check Grade Result:</p>
		<p><?php echo getLetterGrade(75); ?></p>

	</body>


</html>